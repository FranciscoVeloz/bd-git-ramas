﻿--Creación de la base de datos
create database coleccion;

use coleccion;

--Creación tabla libros
create table libros(
	isbn int primary key auto_increment,
	titulo varchar(100),
	num_paginas int,
	autor varchar(100),
	genero varchar(100),
	status varchar(100)
);

--Creación tabla avance
create table avance(
	id int primary key auto_increment,
	fk_libro int,
	num_paginas int,
	fecha varchar(100),
	foreign key(fk_libro) references libros(isbn)
);