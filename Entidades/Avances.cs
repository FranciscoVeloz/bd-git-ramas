﻿namespace Entidades
{
    public class Avances
    {
        public int Id { get; set; }
        public int Id_libro { get; set; }
        public int Num_paginas { get; set; }
        public string Fecha { get; set; }

        public Avances(int id, int id_libro, int num_paginas, string fecha)
        {
            Id = id;
            Id_libro = id_libro;
            Num_paginas = num_paginas;
            Fecha = fecha;
        }
    }
}
