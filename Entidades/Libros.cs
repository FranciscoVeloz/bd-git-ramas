﻿namespace Entidades
{
    public class Libros
    {
        public int ISBN { get; set; }
        public string Titulo { get; set; }
        public int Num_Paginas { get; set; }
        public string Autor { get; set; }
        public string Genero { get; set; }
        public string Status { get; set; }

        public Libros(int isbn, string titulo, int num_Paginas, string autor, string genero, string status)
        {
            ISBN = isbn;
            Titulo = titulo;
            Num_Paginas = num_Paginas;
            Autor = autor;
            Genero = genero;
            Status = status;
        }
    }
}
