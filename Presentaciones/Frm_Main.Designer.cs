﻿namespace Presentacion
{
    partial class Frm_Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Frm_Main));
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.OptLibros = new System.Windows.Forms.ToolStripButton();
            this.OptAvances = new System.Windows.Forms.ToolStripButton();
            this.OptSalir = new System.Windows.Forms.ToolStripButton();
            this.toolStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // toolStrip1
            // 
            this.toolStrip1.Dock = System.Windows.Forms.DockStyle.Left;
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.OptLibros,
            this.OptAvances,
            this.OptSalir});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(69, 457);
            this.toolStrip1.TabIndex = 1;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // OptLibros
            // 
            this.OptLibros.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.OptLibros.Image = ((System.Drawing.Image)(resources.GetObject("OptLibros.Image")));
            this.OptLibros.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.OptLibros.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.OptLibros.Name = "OptLibros";
            this.OptLibros.Size = new System.Drawing.Size(66, 68);
            this.OptLibros.Text = "Libros";
            this.OptLibros.Click += new System.EventHandler(this.OptLibros_Click);
            // 
            // OptAvances
            // 
            this.OptAvances.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.OptAvances.Image = ((System.Drawing.Image)(resources.GetObject("OptAvances.Image")));
            this.OptAvances.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.OptAvances.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.OptAvances.Name = "OptAvances";
            this.OptAvances.Size = new System.Drawing.Size(66, 68);
            this.OptAvances.Text = "Avances";
            this.OptAvances.Click += new System.EventHandler(this.OptAvances_Click);
            // 
            // OptSalir
            // 
            this.OptSalir.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.OptSalir.Image = ((System.Drawing.Image)(resources.GetObject("OptSalir.Image")));
            this.OptSalir.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.OptSalir.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.OptSalir.Name = "OptSalir";
            this.OptSalir.Size = new System.Drawing.Size(66, 68);
            this.OptSalir.Text = "Salir";
            this.OptSalir.Click += new System.EventHandler(this.OptSalir_Click);
            // 
            // Frm_Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 18F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(886, 457);
            this.ControlBox = false;
            this.Controls.Add(this.toolStrip1);
            this.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.IsMdiContainer = true;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "Frm_Main";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Show;
            this.Text = "Colección";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton OptLibros;
        private System.Windows.Forms.ToolStripButton OptAvances;
        private System.Windows.Forms.ToolStripButton OptSalir;
    }
}