﻿using System;
using System.Windows.Forms;

namespace Presentacion
{
    public partial class Frm_Main : Form
    {
        public Frm_Main()
        {
            InitializeComponent();
        }

        private void OptSalir_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
        private void OptLibros_Click(object sender, EventArgs e)
        {
            Frm_libros libros = new Frm_libros();
            libros.ShowDialog();
        }
        private void OptAvances_Click(object sender, EventArgs e)
        {
            Frm_avances avances = new Frm_avances();
            avances.ShowDialog();
        }
    }
}
