﻿using System;
using System.Windows.Forms;
using Entidades;
using Manejadores;

namespace Presentacion
{
    public partial class Frm_libros : Form
    {
        Libros libros = new Libros(0, "", 0, "", "", "");
        ManejadorLibro manejador = new ManejadorLibro();

        int id = 0;

        public Frm_libros()
        {
            InitializeComponent();
        }

        void LimpiarTxt()
        {
            TxtAutor.Clear();
            TxtGenero.Clear();
            TxtNum_Pag.Clear();
            CmbStatus.Text = "";
            TxtTitulo.Clear();
        }

        void ActualizarDtg()
        {
            DtgLibros.DataSource = manejador.Mostrar($"select * from libros", "libros").Tables[0];
            DtgLibros.AutoResizeColumns();
            LimpiarTxt();
        }

        private void Frm_libros_Load(object sender, EventArgs e)
        {
            ActualizarDtg();
        }

        private void OptRegresar_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void OptGuardar_Click(object sender, EventArgs e)
        {
            string resultado = manejador.Insertar(new Libros(0, TxtTitulo.Text, int.Parse(TxtNum_Pag.Text), TxtAutor.Text,
                TxtGenero.Text, CmbStatus.Text));
            ActualizarDtg();
        }

        private void DtgLibros_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            libros.ISBN = int.Parse(DtgLibros.Rows[e.RowIndex].Cells[0].Value.ToString());
            libros.Titulo = DtgLibros.Rows[e.RowIndex].Cells[1].Value.ToString();
            libros.Num_Paginas = int.Parse(DtgLibros.Rows[e.RowIndex].Cells[2].Value.ToString());
            libros.Autor = DtgLibros.Rows[e.RowIndex].Cells[3].Value.ToString();
            libros.Genero = DtgLibros.Rows[e.RowIndex].Cells[4].Value.ToString();
            libros.Status = DtgLibros.Rows[e.RowIndex].Cells[5].Value.ToString();

            id = libros.ISBN;
            TxtTitulo.Text = libros.Titulo;
            TxtNum_Pag.Text = libros.Num_Paginas.ToString();
            TxtAutor.Text = libros.Autor;
            TxtGenero.Text = libros.Genero;
            CmbStatus.Text = libros.Status;
        }

        private void OptActualizar_Click(object sender, EventArgs e)
        {
            string nuevoResultado = manejador.Actualizar(libros = new Libros(id, TxtTitulo.Text, int.Parse(TxtNum_Pag.Text),
                TxtAutor.Text, TxtGenero.Text, CmbStatus.Text));
            ActualizarDtg();
        }

        private void OptEliminar_Click(object sender, EventArgs e)
        {
            DialogResult resultado = MessageBox.Show($"Quieres eliminar al libro: { libros.Titulo}?", "Warning!", MessageBoxButtons.YesNo);

            if (resultado == DialogResult.Yes)
            {
                manejador.Eliminar(libros);
                ActualizarDtg();
            }
        }
    }
}
