﻿namespace Presentacion
{
    partial class Frm_avances
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Frm_avances));
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.OptRegresar = new System.Windows.Forms.ToolStripButton();
            this.OptGuardar = new System.Windows.Forms.ToolStripButton();
            this.OptActualizar = new System.Windows.Forms.ToolStripButton();
            this.OptEliminar = new System.Windows.Forms.ToolStripButton();
            this.label7 = new System.Windows.Forms.Label();
            this.TxtNum_Pag = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.TxtLibro = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.DtgAvance = new System.Windows.Forms.DataGridView();
            this.DtpFecha = new System.Windows.Forms.DateTimePicker();
            this.toolStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DtgAvance)).BeginInit();
            this.SuspendLayout();
            // 
            // toolStrip1
            // 
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.OptRegresar,
            this.OptGuardar,
            this.OptActualizar,
            this.OptEliminar});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Padding = new System.Windows.Forms.Padding(0, 0, 2, 0);
            this.toolStrip1.Size = new System.Drawing.Size(800, 31);
            this.toolStrip1.TabIndex = 12;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // OptRegresar
            // 
            this.OptRegresar.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.OptRegresar.Image = ((System.Drawing.Image)(resources.GetObject("OptRegresar.Image")));
            this.OptRegresar.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.OptRegresar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.OptRegresar.Name = "OptRegresar";
            this.OptRegresar.Size = new System.Drawing.Size(28, 28);
            this.OptRegresar.Text = "toolStripButton1";
            this.OptRegresar.ToolTipText = "Regresar";
            this.OptRegresar.Click += new System.EventHandler(this.OptRegresar_Click);
            // 
            // OptGuardar
            // 
            this.OptGuardar.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.OptGuardar.Image = ((System.Drawing.Image)(resources.GetObject("OptGuardar.Image")));
            this.OptGuardar.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.OptGuardar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.OptGuardar.Name = "OptGuardar";
            this.OptGuardar.Size = new System.Drawing.Size(28, 28);
            this.OptGuardar.Text = "toolStripButton2";
            this.OptGuardar.ToolTipText = "Guardar";
            this.OptGuardar.Click += new System.EventHandler(this.OptGuardar_Click);
            // 
            // OptActualizar
            // 
            this.OptActualizar.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.OptActualizar.Image = ((System.Drawing.Image)(resources.GetObject("OptActualizar.Image")));
            this.OptActualizar.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.OptActualizar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.OptActualizar.Name = "OptActualizar";
            this.OptActualizar.Size = new System.Drawing.Size(28, 28);
            this.OptActualizar.Text = "toolStripButton3";
            this.OptActualizar.ToolTipText = "Actualizar";
            this.OptActualizar.Click += new System.EventHandler(this.OptActualizar_Click);
            // 
            // OptEliminar
            // 
            this.OptEliminar.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.OptEliminar.Image = ((System.Drawing.Image)(resources.GetObject("OptEliminar.Image")));
            this.OptEliminar.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.OptEliminar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.OptEliminar.Name = "OptEliminar";
            this.OptEliminar.Size = new System.Drawing.Size(28, 28);
            this.OptEliminar.Text = "toolStripButton4";
            this.OptEliminar.ToolTipText = "Eliminar";
            this.OptEliminar.Click += new System.EventHandler(this.OptEliminar_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(435, 60);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(52, 18);
            this.label7.TabIndex = 16;
            this.label7.Text = "Fecha";
            // 
            // TxtNum_Pag
            // 
            this.TxtNum_Pag.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.TxtNum_Pag.Location = new System.Drawing.Point(15, 161);
            this.TxtNum_Pag.Name = "TxtNum_Pag";
            this.TxtNum_Pag.Size = new System.Drawing.Size(350, 26);
            this.TxtNum_Pag.TabIndex = 2;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 140);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(191, 18);
            this.label4.TabIndex = 19;
            this.label4.Text = "Número de páginas leidas";
            // 
            // TxtLibro
            // 
            this.TxtLibro.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.TxtLibro.Location = new System.Drawing.Point(15, 81);
            this.TxtLibro.Name = "TxtLibro";
            this.TxtLibro.Size = new System.Drawing.Size(350, 26);
            this.TxtLibro.TabIndex = 1;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 60);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(79, 18);
            this.label3.TabIndex = 20;
            this.label3.Text = "Id del libro";
            // 
            // DtgAvance
            // 
            this.DtgAvance.BackgroundColor = System.Drawing.SystemColors.Control;
            this.DtgAvance.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DtgAvance.Location = new System.Drawing.Point(12, 238);
            this.DtgAvance.Name = "DtgAvance";
            this.DtgAvance.Size = new System.Drawing.Size(776, 300);
            this.DtgAvance.TabIndex = 4;
            this.DtgAvance.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DtgAvance_CellContentClick);
            // 
            // DtpFecha
            // 
            this.DtpFecha.Location = new System.Drawing.Point(438, 81);
            this.DtpFecha.Name = "DtpFecha";
            this.DtpFecha.Size = new System.Drawing.Size(350, 26);
            this.DtpFecha.TabIndex = 3;
            // 
            // Frm_avances
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 18F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ClientSize = new System.Drawing.Size(800, 550);
            this.Controls.Add(this.DtpFecha);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.TxtNum_Pag);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.TxtLibro);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.DtgAvance);
            this.Controls.Add(this.toolStrip1);
            this.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "Frm_avances";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Frm_avances";
            this.Load += new System.EventHandler(this.Frm_avances_Load);
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DtgAvance)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton OptRegresar;
        private System.Windows.Forms.ToolStripButton OptGuardar;
        private System.Windows.Forms.ToolStripButton OptActualizar;
        private System.Windows.Forms.ToolStripButton OptEliminar;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox TxtNum_Pag;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox TxtLibro;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DataGridView DtgAvance;
        private System.Windows.Forms.DateTimePicker DtpFecha;
    }
}