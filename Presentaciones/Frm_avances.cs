﻿using System;
using System.Windows.Forms;
using Entidades;
using Manejadores;

namespace Presentacion
{
    public partial class Frm_avances : Form
    {
        Avances avances = new Avances(0, 0, 0, "");
        ManejadorAvance manejador = new ManejadorAvance();

        int id = 0;

        public Frm_avances()
        {
            InitializeComponent();
        }

        void LimpiarTxt()
        {
            TxtLibro.Clear();
            TxtNum_Pag.Clear();
        }

        void ActualizarDtg()
        {
            DtgAvance.DataSource = manejador.Mostrar($"select * from avance", "avance").Tables[0];
            DtgAvance.AutoResizeColumns();
            LimpiarTxt();
        }

        private void Frm_avances_Load(object sender, EventArgs e)
        {
            ActualizarDtg();
        }

        private void OptRegresar_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void OptGuardar_Click(object sender, EventArgs e)
        {
            string resultado = manejador.Insertar(new Avances(0, int.Parse(TxtLibro.Text), int.Parse(TxtNum_Pag.Text), DtpFecha.Text));
            ActualizarDtg();
        }

        private void DtgAvance_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            avances.Id = int.Parse(DtgAvance.Rows[e.RowIndex].Cells[0].Value.ToString());
            avances.Id_libro = int.Parse(DtgAvance.Rows[e.RowIndex].Cells[1].Value.ToString());
            avances.Num_paginas = int.Parse(DtgAvance.Rows[e.RowIndex].Cells[2].Value.ToString());
            avances.Fecha = DtgAvance.Rows[e.RowIndex].Cells[3].Value.ToString();

            id = avances.Id;
            TxtLibro.Text = avances.Id_libro.ToString();
            TxtNum_Pag.Text = avances.Num_paginas.ToString();
            DtpFecha.Text = avances.Fecha.ToString();
        }

        private void OptActualizar_Click(object sender, EventArgs e)
        {
            string nuevoResultado = manejador.Actualizar(avances = new Avances(id, int.Parse(TxtLibro.Text), 
                int.Parse(TxtNum_Pag.Text), DtpFecha.Text));
            ActualizarDtg();
        }

        private void OptEliminar_Click(object sender, EventArgs e)
        {
            DialogResult resultado = MessageBox.Show($"Quieres eliminar el avance: { avances.Id}?", "Warning!", MessageBoxButtons.YesNo);
            if (resultado == DialogResult.Yes)
            {
                manejador.Eliminar(avances);
                ActualizarDtg();
            }
        }
    }
}
