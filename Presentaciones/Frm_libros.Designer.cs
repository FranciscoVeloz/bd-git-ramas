﻿namespace Presentacion
{
    partial class Frm_libros
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Frm_libros));
            this.DtgLibros = new System.Windows.Forms.DataGridView();
            this.label3 = new System.Windows.Forms.Label();
            this.TxtTitulo = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.TxtNum_Pag = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.TxtAutor = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.TxtGenero = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.OptRegresar = new System.Windows.Forms.ToolStripButton();
            this.OptGuardar = new System.Windows.Forms.ToolStripButton();
            this.OptActualizar = new System.Windows.Forms.ToolStripButton();
            this.OptEliminar = new System.Windows.Forms.ToolStripButton();
            this.CmbStatus = new System.Windows.Forms.ComboBox();
            ((System.ComponentModel.ISupportInitialize)(this.DtgLibros)).BeginInit();
            this.toolStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // DtgLibros
            // 
            this.DtgLibros.BackgroundColor = System.Drawing.SystemColors.Control;
            this.DtgLibros.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DtgLibros.Location = new System.Drawing.Point(12, 288);
            this.DtgLibros.Name = "DtgLibros";
            this.DtgLibros.Size = new System.Drawing.Size(776, 300);
            this.DtgLibros.TabIndex = 10;
            this.DtgLibros.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DtgLibros_CellContentClick);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 60);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(44, 18);
            this.label3.TabIndex = 4;
            this.label3.Text = "Título";
            // 
            // TxtTitulo
            // 
            this.TxtTitulo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.TxtTitulo.Location = new System.Drawing.Point(15, 81);
            this.TxtTitulo.Name = "TxtTitulo";
            this.TxtTitulo.Size = new System.Drawing.Size(350, 26);
            this.TxtTitulo.TabIndex = 2;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 120);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(145, 18);
            this.label4.TabIndex = 4;
            this.label4.Text = "Número de páginas";
            // 
            // TxtNum_Pag
            // 
            this.TxtNum_Pag.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.TxtNum_Pag.Location = new System.Drawing.Point(15, 141);
            this.TxtNum_Pag.Name = "TxtNum_Pag";
            this.TxtNum_Pag.Size = new System.Drawing.Size(350, 26);
            this.TxtNum_Pag.TabIndex = 3;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(12, 180);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(45, 18);
            this.label6.TabIndex = 4;
            this.label6.Text = "Autor";
            // 
            // TxtAutor
            // 
            this.TxtAutor.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.TxtAutor.Location = new System.Drawing.Point(15, 201);
            this.TxtAutor.Name = "TxtAutor";
            this.TxtAutor.Size = new System.Drawing.Size(350, 26);
            this.TxtAutor.TabIndex = 4;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(435, 60);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(60, 18);
            this.label7.TabIndex = 4;
            this.label7.Text = "Género";
            // 
            // TxtGenero
            // 
            this.TxtGenero.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.TxtGenero.Location = new System.Drawing.Point(438, 81);
            this.TxtGenero.Name = "TxtGenero";
            this.TxtGenero.Size = new System.Drawing.Size(350, 26);
            this.TxtGenero.TabIndex = 5;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(435, 120);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(60, 18);
            this.label8.TabIndex = 4;
            this.label8.Text = "Estatus";
            // 
            // toolStrip1
            // 
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.OptRegresar,
            this.OptGuardar,
            this.OptActualizar,
            this.OptEliminar});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(800, 31);
            this.toolStrip1.TabIndex = 11;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // OptRegresar
            // 
            this.OptRegresar.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.OptRegresar.Image = ((System.Drawing.Image)(resources.GetObject("OptRegresar.Image")));
            this.OptRegresar.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.OptRegresar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.OptRegresar.Name = "OptRegresar";
            this.OptRegresar.Size = new System.Drawing.Size(28, 28);
            this.OptRegresar.Text = "toolStripButton1";
            this.OptRegresar.ToolTipText = "Regresar";
            this.OptRegresar.Click += new System.EventHandler(this.OptRegresar_Click);
            // 
            // OptGuardar
            // 
            this.OptGuardar.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.OptGuardar.Image = ((System.Drawing.Image)(resources.GetObject("OptGuardar.Image")));
            this.OptGuardar.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.OptGuardar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.OptGuardar.Name = "OptGuardar";
            this.OptGuardar.Size = new System.Drawing.Size(28, 28);
            this.OptGuardar.Text = "toolStripButton2";
            this.OptGuardar.ToolTipText = "Guardar";
            this.OptGuardar.Click += new System.EventHandler(this.OptGuardar_Click);
            // 
            // OptActualizar
            // 
            this.OptActualizar.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.OptActualizar.Image = ((System.Drawing.Image)(resources.GetObject("OptActualizar.Image")));
            this.OptActualizar.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.OptActualizar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.OptActualizar.Name = "OptActualizar";
            this.OptActualizar.Size = new System.Drawing.Size(28, 28);
            this.OptActualizar.Text = "toolStripButton3";
            this.OptActualizar.ToolTipText = "Actualizar";
            this.OptActualizar.Click += new System.EventHandler(this.OptActualizar_Click);
            // 
            // OptEliminar
            // 
            this.OptEliminar.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.OptEliminar.Image = ((System.Drawing.Image)(resources.GetObject("OptEliminar.Image")));
            this.OptEliminar.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.OptEliminar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.OptEliminar.Name = "OptEliminar";
            this.OptEliminar.Size = new System.Drawing.Size(28, 28);
            this.OptEliminar.Text = "toolStripButton4";
            this.OptEliminar.ToolTipText = "Eliminar";
            this.OptEliminar.Click += new System.EventHandler(this.OptEliminar_Click);
            // 
            // CmbStatus
            // 
            this.CmbStatus.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CmbStatus.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.CmbStatus.FormattingEnabled = true;
            this.CmbStatus.Items.AddRange(new object[] {
            "Pendiente",
            "Proceso",
            "Leído"});
            this.CmbStatus.Location = new System.Drawing.Point(438, 141);
            this.CmbStatus.Name = "CmbStatus";
            this.CmbStatus.Size = new System.Drawing.Size(350, 26);
            this.CmbStatus.TabIndex = 6;
            // 
            // Frm_libros
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 18F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 600);
            this.Controls.Add(this.CmbStatus);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.TxtGenero);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.TxtAutor);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.TxtNum_Pag);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.TxtTitulo);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.DtgLibros);
            this.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "Frm_libros";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Frm_libros";
            this.Load += new System.EventHandler(this.Frm_libros_Load);
            ((System.ComponentModel.ISupportInitialize)(this.DtgLibros)).EndInit();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.DataGridView DtgLibros;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox TxtTitulo;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox TxtNum_Pag;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox TxtAutor;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox TxtGenero;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton OptRegresar;
        private System.Windows.Forms.ToolStripButton OptGuardar;
        private System.Windows.Forms.ToolStripButton OptActualizar;
        private System.Windows.Forms.ToolStripButton OptEliminar;
        private System.Windows.Forms.ComboBox CmbStatus;
    }
}