﻿using System.Data;
using AccesoDatos;
using Entidades;

namespace Manejadores
{
    public class ManejadorLibro
    {
        Conexion conexion = new Conexion();

        ////Insertar datos
        public string Insertar(Libros libros)
        {
            return conexion.Comando($"insert into libros values(null, '{libros.Titulo}', '{libros.Num_Paginas}', " +
                $"'{libros.Autor}', '{libros.Genero}', '{libros.Status}')");
        }

        ////Eliminar datos
        public string Eliminar(Libros libros)
        {
            return conexion.Comando($"delete from libros where isbn = '{libros.ISBN}'");
        }

        ////Actualizar datos
        public string Actualizar(Libros libros)
        {
            return conexion.Comando($"update libros set titulo = '{libros.Titulo}', num_paginas = '{libros.Num_Paginas}', " +
                $"autor = '{libros.Autor}', genero = '{libros.Genero}', status = '{libros.Status}' where isbn = '{libros.ISBN}'");
        }

        ////Mostrar datos
        public DataSet Mostrar(string query, string table)
        {
            return conexion.Mostrar(query, table);
        }
    }
}
