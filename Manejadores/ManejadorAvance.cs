﻿using System.Data;
using AccesoDatos;
using Entidades;

namespace Manejadores
{
    public class ManejadorAvance
    {
        Conexion conexion = new Conexion();

        ////Insertar datos
        public string Insertar(Avances avances)
        {
            return conexion.Comando($"insert into avance values(null, '{avances.Id_libro}', '{avances.Num_paginas})', '{avances.Fecha}')");
        }

        ////Eliminar datos
        public string Eliminar(Avances avances)
        {
            return conexion.Comando($"delete from avance where id = '{avances.Id}'");
        }

        ////Actualizar datos
        public string Actualizar(Avances avances)
        {
            return conexion.Comando($"update avance set fk_libro = '{avances.Id_libro}', num_paginas = '{avances.Num_paginas}', " +
                $"fecha = '{avances.Fecha}' where id = '{avances.Id}'");
        }

        ////Mostrar datos
        public DataSet Mostrar(string query, string table)
        {
            return conexion.Mostrar(query, table);
        }
    }
}
